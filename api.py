import os
from flask import Flask, jsonify, request
from db import *
from flask_cors import CORS
from settings import sudo_password

app = Flask(__name__)
app.config['SECRET_KEY'] = 'hSlwSo23OXksd#$jsnFMSl92382'

CORS(app)


@app.route("/sentiment/data")
def data():
    return jsonify({'positive': [item['positive'] for item in get_graph_data()],
                    'negative': [item['negative'] for item in get_graph_data()],
                    'bitcoin_price': [item['bitcoin_price'] for item in get_graph_data()],
                    'timestamp': [item['timestamp'] for item in get_graph_data()]
                    })


@app.route("/")
def home():
    return jsonify({'status': 'OK'})


@app.route("/admin/data")
def admin():
    data = get_database_data()
    return jsonify(data)


@app.route("/admin/database/new", methods=['POST'])
def create_database():
    result = new_database(request.form['name'])
    return jsonify({'message': result})


@app.route("/admin/database/delete-by-date", methods=['POST'])
def delete_data_by_date():
    result = clear_cashtag_by_date(request.form['date'])
    return jsonify({'message': result})

@app.route("/admin/database/clear")
def delete_data_by_date():
    result = clear_cashtag()
    return jsonify({'message': result})


@app.route("/admin/twitter/start")
def twitter_start():
    os.system("echo '%s' | systemctl start twitter_bot" % sudo_password)
    return jsonify({'message': 'Success!'})


@app.route("/admin/twitter/stop")
def twitter_stop():
    os.system("echo '%s' | systemctl stop twitter_bot" % sudo_password)
    return jsonify({'message': 'Success!'})


@app.route("/admin/twitter/restart")
def twitter_restart():
    os.system("echo '%s' | systemctl restart twitter_bot" % sudo_password)
    return jsonify({'message': 'Success!'})


@app.route("/admin/api/start")
def api_start():
    os.system("echo '%s' | systemctl start gunicorn" % sudo_password)
    return jsonify({'message': 'Success!'})


@app.route("/admin/api/stop")
def api_stop():
    os.system("echo '%s' | systemctl stop gunicorn" % sudo_password)
    return jsonify({'message': 'Success!'})


@app.route("/admin/api/restart")
def api_restart():
    os.system("echo '%s' | systemctl restart gunicorn" % sudo_password)
    return jsonify({'message': 'Success!'})


if __name__ == '__main__':
    app.run(debug=True)
