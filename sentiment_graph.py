# -*- coding: utf-8 -*-
import plotly

plotly.tools.set_credentials_file(username='neiellcare', api_key='yLg21k5JVrNn0g31IgEr')
import plotly.plotly as py
from plotly.graph_objs import *
from db import get_graph_data

positive = Scatter(
    x=[item['timestamp'] for item in get_graph_data()],
    y=[item['positive'] for item in get_graph_data()],
    name = 'Positive Sentiment'

)
negative = Scatter(
    x=[item['timestamp'] for item in get_graph_data()],
    y=[item['negative'] for item in get_graph_data()],
    name = 'Negative Sentiment'

)
data = Data([positive, negative])

py.plot(data, filename = 'basic-line')