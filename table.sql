-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema cashtag
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema cashtag
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `cashtag` DEFAULT CHARACTER SET utf8 ;
USE `cashtag` ;

-- -----------------------------------------------------
-- Table `cashtag`.`Mentions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cashtag`.`Mentions` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `count` INT NULL,
  `bitcoin_price` FLOAT NULL,
  `cashtag_name` VARCHAR(45) NULL,
  `created_time` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
