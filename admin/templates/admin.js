$(document).ready(function () {
    databaseList = {};
    $.ajax({
        async: false,
        url: 'http://localhost:5000/admin/data',
        dataType: 'json',
        type: 'GET',
        success: function (data) {
            databaseList = data;
        }
    });
    html = "";
    $.each(databaseList, function (id, obj) {
        html += '<div class="form-group row">\n' +
            '                    <label class="col-sm-2 col-form-label">' + obj.name + '</label>\n' +
            '                    <div class="col-sm-10">\n' +
            obj.size + ' mb' +
            '                    </div>\n' +
            '                </div>'
    });

    $("#listOfDatabase").append(html);
});

$(document).on('click', "button#clearData", function () {
    var r = confirm("Do you want to delete all Data?");
    if (r === true) {

    }
});

$(document).on('click', "button#apiStart", function () {
    $.ajax({
        async: false,
        url: 'http://localhost:5000/admin/api/start',
        dataType: 'json',
        type: 'GET',
        success: function (data) {
            alert(data['message']);
        }
    });
});

$(document).on('click', "button#apiStop", function () {
    $.ajax({
        async: false,
        url: 'http://localhost:5000/admin/api/stop',
        dataType: 'json',
        type: 'GET',
        success: function (data) {
            alert(data['message']);
        }
    });
});

$(document).on('click', "button#apiRestart", function () {
    $.ajax({
        async: false,
        url: 'http://localhost:5000/admin/api/restart',
        dataType: 'json',
        type: 'GET',
        success: function (data) {
            alert(data['message']);
        }
    });
});

$(document).on('click', "button#botStart", function () {
    $.ajax({
        async: false,
        url: 'http://localhost:5000/admin/twitter/start',
        dataType: 'json',
        type: 'GET',
        success: function (data) {
            alert(data['message']);
        }
    });
});

$(document).on('click', "button#botStop", function () {
    $.ajax({
        async: false,
        url: 'http://localhost:5000/admin/twitter/stop',
        dataType: 'json',
        type: 'GET',
        success: function (data) {
            alert(data['message']);
        }
    });
});

$(document).on('click', "button#botRestart", function () {
    $.ajax({
        async: false,
        url: 'http://localhost:5000/admin/twitter/restart',
        dataType: 'json',
        type: 'GET',
        success: function (data) {
            alert(data['message']);
        }
    });
});

$(document).on('click', "button#submitDb", function () {
    formData = {
        'name' : $("#databaseName").val()
    };

    $.ajax({
        async: false,
        url: 'http://localhost:5000/admin/database/new',
        dataType: 'json',
        type: 'POST',
        data: formData,
        success: function (data) {
            alert(data['message']);
        }
    });
});
$(document).on('click', "button#submitDate", function () {
    formData = {
        'date' : $("#specific_date").val()
    };

    $.ajax({
        async: false,
        url: 'http://localhost:5000/admin/database/delete-by-date',
        dataType: 'json',
        type: 'POST',
        data: formData,
        success: function (data) {
            alert(data['message']);
        }
    });
});

$(document).on('click', "button#clearData", function () {
    $.ajax({
        async: false,
        url: 'http://localhost:5000/admin/database/clear',
        dataType: 'json',
        type: 'GET',
        success: function (data) {
            alert(data['message']);
        }
    });
});
