# -*- coding: utf-8 -*-
import json
import sys
import urllib2

import time
import tweepy
import codecs

from twitter_stream_listener import TwitterStreamListener
from sentiment_analysis import TwitterSentimentAnalysis

sys.stdout = codecs.getwriter("iso-8859-1")(sys.stdout, 'xmlcharrefreplace')

from settings import *
from db import store_mentions, store_sentiments
from daemon import Daemon

# Twitter Credentials
auth = tweepy.OAuthHandler(CUSTOMER_KEY, CUSTOMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)


class TwitterBot(Daemon):
    def __init__(self, cashtag="bitcoin"):
        Daemon.__init__(self, pidfile='/tmp/daemon-example.pid')
        self.api = tweepy.API(auth)
        self.cashtag = cashtag

    def bitcoin_price(self):
        # Requesting the real-time bitcoin price url
        request = urllib2.Request("https://api.coindesk.com/v1/bpi/currentprice.json")
        response = urllib2.urlopen(request)

        # converting to json
        data = json.loads(response.read())
        return data['bpi']['USD']['rate_float']

    def run(self):
        # cashtag = ["$btcusd"]
        # cashtag = ["$btcusd", "#bitcoin"]
        listener = TwitterStreamListener()
        sentiment_analyzer = TwitterSentimentAnalysis()
        stream = tweepy.Stream(auth=auth, listener=listener)
        stream.filter(track=[self.cashtag], async=True)

        while True:
            time.sleep(30)
            print(listener.get_number_of_mentions())

            positive_sentiment = 0
            negative_sentiment = 0

            if listener.get_number_of_mentions() > 0:
                tweets = sentiment_analyzer.get_sentiment(listener.get_mentions())

                # picking positive tweets from tweets
                ptweets = [tweet for tweet in tweets if tweet['sentiment'] == 'positive']

                # picking negative tweets from tweets
                ntweets = [tweet for tweet in tweets if tweet['sentiment'] == 'negative']

                # get number per sentiments
                positive_sentiment = len(ptweets)
                negative_sentiment = len(ntweets)

            # Storing to Database
            id = store_mentions(self.cashtag, listener.get_number_of_mentions(), self.bitcoin_price())
            store_sentiments(positive_sentiment, negative_sentiment, id)

            # Reset number of mentions
            listener.reset_counter()

if __name__ == "__main__":
    bot = TwitterBot()
    if 'start' == sys.argv[1]:
        #bot.start()
        bot.run()  # for testing to see logs
    elif 'stop' == sys.argv[1]:
        bot.stop()
    elif 'restart' == sys.argv[1]:
        bot.restart()
    else:
        print("Unknown command")
        sys.exit(2)
