# -*- coding: utf-8 -*-
import plotly

plotly.tools.set_credentials_file(username='neiellcare', api_key='yLg21k5JVrNn0g31IgEr')
import plotly.plotly as py
from plotly.graph_objs import *
from db import get_mentions_data

btcusd_mentions = Scatter(
    x=[item['timestamp'] for item in get_mentions_data()],
    y=[item['count'] for item in get_mentions_data()]

)
bitcoin = Scatter(
    x=[item['timestamp'] for item in get_mentions_data()],
    y=[item['bitcoin_price'] for item in get_mentions_data()]

)
bitcoin_mentions = Scatter(
    x=[item['timestamp'] for item in get_mentions_data()],
    y=[item['bitcoin_price'] for item in get_mentions_data()]

)
data = Data([btcusd_mentions, bitcoin_mentions, bitcoin])

py.plot(data, filename = 'basic-line')